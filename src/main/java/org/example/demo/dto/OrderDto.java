package org.example.demo.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@Builder
public class OrderDto {
    private UUID id;
    private UUID user_id;
    private UUID book_id;
    private int quantity;
    private boolean confirmed;
}

