package org.example.demo.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@FeignClient(name = "notificationFeignClient", url = "http://localhost:8085/abbtech-bookstore-book-ms/")
public interface NotificationFeignClient {
    @GetMapping("/")
    ResponseEntity<String> getBook(@RequestParam("bookId") UUID bookId);
}
