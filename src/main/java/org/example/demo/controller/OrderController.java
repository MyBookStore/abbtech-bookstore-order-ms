package org.example.demo.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.example.demo.dto.OrderDto;
import org.example.demo.model.Order;
import org.example.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.util.UUID;


@RestController
@RequestMapping("/")
@RequiredArgsConstructor
@Validated
public class OrderController {
      private final OrderService orderService;
    private final NotificationFeignClient notificationFeignClient;

    @PostMapping("create")
    public ResponseEntity<String> createOrder(@RequestBody @Valid OrderDto orderDto,
                                              @RequestParam("bookId") UUID bookId ){

        System.out.println("order confirmed");
        ResponseEntity<String> resp = notificationFeignClient.getBook(bookId);
        System.out.println(resp);
        return orderService.createOrder(orderDto,bookId);}

    @PostMapping("/confirm/{id}")
    public Order confirmOrder(@PathVariable UUID id) {
        return orderService.confirmOrder(id);
    }
    @GetMapping("{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable UUID id){
        Order order = orderService.getById(id);
        return ResponseEntity.ok(order);
    }


//    @GetMapping("/confirm")
//    public void confirmOrder(@RequestParam("orderId") String orderId) {
//        System.out.println("order confirmed");
//        String resp = notificationFeignClient.sendNotification(orderId);
//        System.out.println(resp);
//    }
}
