package org.example.demo.service;

import org.example.demo.dto.OrderDto;
import org.example.demo.model.Order;
import org.example.demo.repository.OrderRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
@Service
public class OrderServiceImpl implements OrderService{
    private final OrderRepository orderRepository;
    private UUID id;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }
    @Override
    public ResponseEntity<String> createOrder(OrderDto orderDto, UUID bookId) {
        Order order=Order.builder()
                .book_id(bookId)
                .user_id(orderDto.getUser_id())
                .quantity(orderDto.getQuantity())
                .confirmed(false)
                .build();


        orderRepository.save(order);
        return ResponseEntity.ok(order.getId()+" Order Created");
    }
    public Order confirmOrder(UUID orderId) {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();
            order.setConfirmed(true);
            return orderRepository.save(order);
        } else {
            throw new RuntimeException("Order not found");
        }
    }

    @Override
    public Order getById(UUID id) {
        return orderRepository.findById(id).orElseThrow(() -> new RuntimeException("Order not found"));
    }
}
