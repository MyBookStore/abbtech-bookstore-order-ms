package org.example.demo.service;

import jakarta.validation.Valid;
import org.example.demo.dto.OrderDto;
import org.example.demo.model.Order;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

public interface OrderService {
    ResponseEntity<String> createOrder(@Valid OrderDto order,UUID bookId);
    public Order confirmOrder(UUID orderId);
    public Order getById( @Valid UUID id);
}
