package org.example.demo.model;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "Book_order",schema="public")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Order{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "UUID")
    private UUID id;
    @Column(name = "book_id", columnDefinition = "UUID")
    private UUID book_id;
    @Column(name = "user_id", columnDefinition = "UUID")
    private UUID user_id;
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "confirmed")
    private boolean confirmed;
}
